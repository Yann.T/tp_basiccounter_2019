﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ClassLibrary;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTestCounter
    {
        [TestMethod]
        public void TestSetter()
        {
            Counter.setValeur(5);
            Assert.AreEqual(Counter.getValeur, 5);

            Counter.setValeur(0);
            Assert.AreEqual(Counter.getValeur, 0);

            Counter.setValeur(999);
            Assert.AreEqual(Counter.getValeur, 999);
        }

        [TestMethod]
        public void TestIncr()
        {
            Counter.setValeur(5);
            Counter.Incrementation();
            Assert.AreEqual(Counter.getValeur, 6);

            Counter.setValeur(0);
            Counter.Incrementation();
            Assert.AreEqual(Counter.getValeur, 1);
        }

        [TestMethod]
        public void TestDecr()
        {
            Counter.setValeur(5);
            Counter.Decrementation();
            Assert.AreEqual(Counter.getValeur, 4);

            Counter.setValeur(0);
            Counter.Decrementation();
            Assert.AreEqual(Counter.getValeur, 0);
        }

        [TestMethod]
        public void TestRAZ()
        {
            Counter.setValeur(5);
            Counter.RAZ();
            Assert.AreEqual(Counter.getValeur, 0);

            Counter.setValeur(0);
            Counter.RAZ();
            Assert.AreEqual(Counter.getValeur, 0);
        }
    }
}
