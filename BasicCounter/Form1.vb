﻿Imports ClassLibrary

Public Class Form1

    Private Sub btn_plus_Click(sender As Object, e As EventArgs) Handles btn_plus.Click
        Counter.Incrementation()
        lbl_valeur.Text = Counter.getValeur
    End Sub

    Private Sub btn_minus_Click(sender As Object, e As EventArgs) Handles btn_minus.Click
        Counter.Decrementation()
        lbl_valeur.Text = Counter.getValeur
    End Sub

    Private Sub btn_raz_Click(sender As Object, e As EventArgs) Handles btn_raz.Click
        Counter.RAZ()
        lbl_valeur.Text = Counter.getValeur
    End Sub
End Class
